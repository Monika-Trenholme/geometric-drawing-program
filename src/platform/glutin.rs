use super::{Event, Platform, Renderer, SystemEventLoop, SystemEventLoopWindowTarget, Vector};
use glutin::config::{Config, ConfigTemplateBuilder};
use glutin::context::{
    ContextApi, ContextAttributesBuilder, NotCurrentContext, PossiblyCurrentContext,
};
use glutin::display::GetGlDisplay;
use glutin::prelude::*;
use glutin::surface::{Surface, SwapInterval, WindowSurface};
use glutin_winit::{DisplayBuilder, GlWindow};
use std::num::NonZeroU32;
use winit::window::{Window, WindowBuilder};

pub struct NativePlatform {
    title: &'static str,
    config: Config,
    not_current_context: Option<NotCurrentContext>,
    state: Option<(Window, Surface<WindowSurface>, PossiblyCurrentContext)>,
    renderer: Option<Renderer>,
    event_loop: Option<SystemEventLoop<()>>,
}

impl Platform for NativePlatform {
    fn init(title: &'static str, event_loop: SystemEventLoop<()>) -> Self {
        unsafe {
            let template = ConfigTemplateBuilder::new()
                .with_alpha_size(8)
                .with_transparency(true);
            let display_builder = DisplayBuilder::new().with_window_builder(None);
            let (_, config) = display_builder
                .build(&event_loop, template, |configs| {
                    configs
                        .reduce(|acc, config| {
                            let transparency_check =
                                config.supports_transparency().unwrap_or(false)
                                    & !acc.supports_transparency().unwrap_or(false);
                            if transparency_check || config.num_samples() > acc.num_samples() {
                                config
                            } else {
                                acc
                            }
                        })
                        .unwrap()
                })
                .unwrap();
            let display = config.display();
            let context_attributes = ContextAttributesBuilder::new().build(None);
            let fallback_context_attributes = ContextAttributesBuilder::new()
                .with_context_api(ContextApi::Gles(None))
                .build(None);
            let not_current_context = Some(
                display
                    .create_context(&config, &context_attributes)
                    .unwrap_or_else(|_| {
                        display
                            .create_context(&config, &fallback_context_attributes)
                            .expect("failed to create context")
                    }),
            );

            Self {
                title,
                config,
                not_current_context,
                state: None,
                renderer: None,
                event_loop: Some(event_loop),
            }
        }
    }

    fn resumed(&mut self, window_target: &SystemEventLoopWindowTarget<()>) {
        unsafe {
            let window_builder = WindowBuilder::new()
                .with_title(self.title)
                .with_inner_size(winit::dpi::LogicalSize::new(512.0, 512.0))
                .with_transparent(true);
            let window =
                glutin_winit::finalize_window(window_target, window_builder, &self.config).unwrap();
            let attrs = window.build_surface_attributes(<_>::default());
            let display = self.config.display();
            let surface = display.create_window_surface(&self.config, &attrs).unwrap();

            // Make it current.
            let context = self
                .not_current_context
                .take()
                .unwrap()
                .make_current(&surface)
                .unwrap();

            // Try setting vsync.
            if let Err(res) =
                surface.set_swap_interval(&context, SwapInterval::Wait(NonZeroU32::new(1).unwrap()))
            {
                eprintln!("Error setting vsync: {res:?}");
            }

            self.renderer = Some(Renderer::new(
                glow::Context::from_loader_function_cstr(move |s| {
                    display.get_proc_address(s) as *const _
                }),
                if cfg!(target_os = "android") {
                    "#version 300 es"
                } else {
                    "#version 330"
                },
            ));

            assert!(self.state.replace((window, surface, context)).is_none());
        }
    }

    fn suspended(&mut self) {
        self.renderer = None;
        let (_, _, context) = self.state.take().unwrap();
        assert!(self
            .not_current_context
            .replace(context.make_not_current().unwrap())
            .is_none());
    }

    fn request_redraw(&self) {
        if let Some((window, _, _)) = &self.state {
            window.request_redraw();
        }
    }

    fn render<C>(&mut self, callback: &mut C)
    where
        C: 'static + FnMut(Event<'_>),
    {
        if let Some(renderer) = &mut self.renderer {
            renderer.clear();
            (callback)(Event::Render(renderer));
        }
        if let Some((_, surface, context)) = &self.state {
            surface.swap_buffers(&context).unwrap();
        }
    }

    fn resize(&self, size: winit::dpi::PhysicalSize<u32>) {
        if let Some((_, surface, context)) = &self.state {
            surface.resize(
                &context,
                NonZeroU32::new(size.width).unwrap(),
                NonZeroU32::new(size.height).unwrap(),
            );
        }
        if let Some(renderer) = &self.renderer {
            renderer.viewport(0, 0, size.width as i32, size.height as i32);
        }
    }

    fn size(&self) -> Option<Vector> {
        if let Some((window, _, _)) = &self.state {
            let size = window.inner_size();
            Some(Vector::new(size.width as f64, size.height as f64))
        } else {
            None
        }
    }

    fn take_event_loop(&mut self) -> SystemEventLoop<()> {
        self.event_loop.take().unwrap()
    }
}
