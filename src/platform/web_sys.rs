use super::{Event, Platform, Renderer, SystemEventLoop, SystemEventLoopWindowTarget, Vector};
use glow::*;
use std::{cell::RefCell, rc::Rc};
use wasm_bindgen::{closure::Closure, JsCast};
use winit::event_loop::EventLoop;
use winit::platform::web::WindowExtWebSys;
use winit::window::{Window, WindowBuilder};

pub struct NativePlatform {
    window: Rc<RefCell<Window>>,
    renderer: Renderer,
    event_loop: Option<SystemEventLoop<()>>,
}

impl Platform for NativePlatform {
    fn init(title: &'static str, event_loop: SystemEventLoop<()>) -> Self {
        unsafe {
            let winit_window = WindowBuilder::new()
                .with_title(title)
                .build(&event_loop)
                .unwrap();
            let canvas = winit_window.canvas();
            let web_sys_window = web_sys::window().unwrap();
            let width = web_sys_window.inner_width().unwrap().as_f64().unwrap();
            let height = web_sys_window.inner_height().unwrap().as_f64().unwrap();
            web_sys_window
                .document()
                .unwrap()
                .body()
                .unwrap()
                .append_child(&canvas)
                .unwrap();
            winit_window.set_inner_size(winit::dpi::PhysicalSize::new(width, height));

            let webgl2_context = canvas
                .get_context("webgl2")
                .unwrap()
                .unwrap()
                .dyn_into::<web_sys::WebGl2RenderingContext>()
                .unwrap();
            let gl = Context::from_webgl2_context(webgl2_context);
            let mut renderer = Renderer::new(gl, "#version 300 es");

            let rc_winit_window = Rc::new(RefCell::new(winit_window));
            let rc_web_sys_window = Rc::new(RefCell::new(web_sys_window));

            let winit_window = rc_winit_window.clone();
            let web_sys_window = rc_web_sys_window.clone();
            let on_resize: Closure<dyn FnMut()> = Closure::new(move || {
                winit_window
                    .borrow()
                    .set_inner_size(winit::dpi::PhysicalSize::new(
                        web_sys_window
                            .borrow()
                            .inner_width()
                            .unwrap()
                            .as_f64()
                            .unwrap(),
                        web_sys_window
                            .borrow()
                            .inner_height()
                            .unwrap()
                            .as_f64()
                            .unwrap(),
                    ))
            });
            rc_web_sys_window
                .borrow()
                .set_onresize(Some(on_resize.as_ref().unchecked_ref()));
            on_resize.forget();

            Self {
                window: rc_winit_window,
                renderer,
                event_loop: Some(event_loop),
            }
        }
    }

    fn resumed(&mut self, _: &SystemEventLoopWindowTarget<()>) {}

    fn suspended(&mut self) {}

    fn request_redraw(&self) {
        self.window.borrow().request_redraw();
    }

    fn render<C>(&mut self, callback: &mut C)
    where
        C: 'static + FnMut(Event<'_>),
    {
        self.renderer.clear();
        (callback)(Event::Render(&mut self.renderer));
    }

    fn resize(&self, size: winit::dpi::PhysicalSize<u32>) {
        self.renderer
            .viewport(0, 0, size.width as i32, size.height as i32);
    }

    fn size(&self) -> Option<Vector> {
        let size = self.window.borrow().inner_size();
        Some(Vector::new(size.width as f64, size.height as f64))
    }

    fn take_event_loop(&mut self) -> SystemEventLoop<()> {
        self.event_loop.take().unwrap()
    }
}
