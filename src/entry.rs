#[cfg(target_os = "android")]
use crate::{
    math::{Element, Vector},
    platform::{Event, NativePlatform, Platform},
    renderer::{Colour, Renderer},
};
#[cfg(not(target_os = "android"))]
use geo_construction::{
    math::{Element, Vector},
    platform::{Event, NativePlatform, Platform},
    renderer::{Colour, Renderer},
};

const WHITE: Colour = Colour(1.0, 1.0, 1.0);
const GRAY: Colour = Colour(0.5, 0.5, 0.5);
const BLACK: Colour = Colour(0.0, 0.0, 0.0);
const RED: Colour = Colour(1.0, 0.0, 0.0);
const ORANGE: Colour = Colour(1.0, 0.5, 0.0);
const YELLOW: Colour = Colour(1.0, 1.0, 0.0);
const GREEN: Colour = Colour(0.0, 1.0, 0.0);
const BLUE: Colour = Colour(0.0, 0.0, 1.0);
const PURPLE: Colour = Colour(0.5, 0.0, 1.0);
const MAGENTA: Colour = Colour(1.0, 0.0, 1.0);

enum Mode {
    Ruler(Option<Vector>),
    Compass(Option<Vector>),
    Eraser,
}

struct Layer {
    colour: Colour,
    elements: Vec<Element>,
    enabled: bool,
}

struct ScreenTransform {
    resolution: Vector,
    square_scale: Vector,
    user_scale: f64,
    translation: Vector,
}

impl ScreenTransform {
    fn array(&self) -> [f64; 9] {
        let s = self.square_scale * self.user_scale;
        let t = self.translation;
        [s.x, 0.0, t.x, 0.0, s.y, t.y, 0.0, 0.0, 1.0]
    }

    fn array_no_user(&self) -> [f64; 9] {
        let s = self.square_scale;
        [s.x, 0.0, 0.0, 0.0, s.y, 0.0, 0.0, 0.0, 1.0]
    }

    fn render_width(&self) -> f64 {
        0.01 / self.user_scale
    }

    fn render_width_no_user(&self) -> f64 {
        0.01
    }

    fn input_width(&self) -> f64 {
        0.1 / self.user_scale
    }
}

struct Cursor {
    dragging: bool,
    screen: Vector,
    graph: Vector,
    snapping: bool,
    hovering: Vec<(usize, usize)>,
}

impl Cursor {
    fn update_position(&mut self, screen_transform: &mut ScreenTransform, screen_position: Vector) {
        let mut uv = screen_position / screen_transform.resolution;
        uv.x = uv.x * 2.0 - 1.0;
        uv.y = -(uv.y * 2.0 - 1.0);
        self.screen = uv;
        self.graph = (self.screen - screen_transform.translation)
            / screen_transform.square_scale
            / screen_transform.user_scale;
    }

    fn update_interaction(
        &mut self,
        screen_transform: &ScreenTransform,
        layers: &Vec<Layer>,
        mode: &Mode,
        ortho: bool,
    ) {
        let width = screen_transform.input_width();

        // Hovering
        self.hovering = layers
            .iter()
            .enumerate()
            .filter(|(_, layer)| layer.enabled)
            .flat_map(|(l, layer)| {
                layer
                    .elements
                    .iter()
                    .enumerate()
                    .map(move |(i, e)| (l, i, e))
            })
            .filter_map(|(l, i, e)| {
                if e.contains_point(width, self.graph) {
                    Some((l, i))
                } else {
                    None
                }
            })
            .collect::<Vec<(usize, usize)>>();

        // Snapping
        self.snapping = false;
        if self.hovering.len() >= 2 {
            let intersection = self
                .hovering
                .iter()
                .enumerate()
                .flat_map(|(ii, (l, i))| {
                    (&self.hovering[ii + 1..])
                        .iter()
                        .map(|&(m, j)| &layers[*l].elements[*i] & &layers[m].elements[j])
                })
                .flat_map(|v| v.into_iter())
                .filter(|&point| (point - self.graph).mag_sq() <= width.powi(2))
                .reduce(|acc, point| {
                    if (point - self.graph).mag_sq() < (acc - self.graph).mag_sq() {
                        point
                    } else {
                        acc
                    }
                });
            if let Some(point) = intersection {
                self.snapping = true;
                self.graph = point;
            }
        }

        // Ortho
        match (mode, ortho) {
            (&Mode::Ruler(Some(previous)) | &Mode::Compass(Some(previous)), true) => {
                let delta = self.graph - previous;
                if delta.x.abs() > delta.y.abs() {
                    self.graph.y = previous.y;
                } else {
                    self.graph.x = previous.x;
                }
            }
            _ => (),
        }
    }
}

#[derive(Clone, Copy)]
enum Layout {
    Landscape,
    Portrait,
}

pub fn enter(platform: NativePlatform) {
    let mut screen_transform = ScreenTransform {
        resolution: Vector::new(0.0, 0.0),
        square_scale: Vector::new(1.0, 1.0),
        user_scale: 1.0,
        translation: Vector::new(0.0, 0.0),
    };

    let mut cursor = Cursor {
        dragging: false,
        screen: Vector::new(0.0, 0.0),
        graph: Vector::new(0.0, 0.0),
        snapping: false,
        hovering: Vec::new(),
    };

    let mut mode = Mode::Ruler(None);
    let mut ortho = false;
    let mut current_layer = 0;
    let mut layers = [WHITE, RED, ORANGE, YELLOW, GREEN, BLUE, PURPLE]
        .into_iter()
        .map(|colour| Layer {
            colour,
            elements: Vec::new(),
            enabled: true,
        })
        .collect::<Vec<Layer>>();

    let mut layout = Layout::Landscape;

    platform.run(move |event| match event {
        Event::Render(renderer) => {
            let width = screen_transform.render_width();
            let transform = screen_transform.array();

            // Layers
            for layer in &layers {
                if layer.enabled {
                    renderer.render_layer(&layer.elements, width, layer.colour, transform);
                }
            }

            // Selection
            renderer.render_layer(
                &match mode {
                    Mode::Eraser => cursor
                        .hovering
                        .iter()
                        .map(|&(l, i)| layers[l].elements[i])
                        .collect(),
                    Mode::Ruler(Some(point)) => vec![
                        Element::circle(point, (width / 2.0).powi(2)),
                        Element::line_segment(point, cursor.graph),
                    ],
                    Mode::Compass(Some(center)) => vec![
                        Element::circle(center, (width / 2.0).powi(2)),
                        Element::circle(center, (cursor.graph - center).mag_sq()),
                    ],
                    _ => Vec::new(),
                },
                width,
                MAGENTA,
                transform,
            );

            // Snap node
            if cursor.snapping {
                renderer.render_layer(
                    &vec![
                        Element::line_segment(
                            cursor.graph + Vector::new(-width * 2.0, width * 2.0),
                            cursor.graph + Vector::new(width * 2.0, -width * 2.0),
                        ),
                        Element::line_segment(
                            cursor.graph + Vector::new(-width * 2.0, -width * 2.0),
                            cursor.graph + Vector::new(width * 2.0, width * 2.0),
                        ),
                    ],
                    width,
                    GRAY,
                    transform,
                );
            }

            // Buttons
            let n = 10;
            let size = 2.0 / n as f64;
            let padding = size / 10.0;
            let transform = screen_transform.array_no_user();
            let (button_delta, padding_delta, background_stroke_delta) = match layout {
                Layout::Landscape => (
                    Vector::new(0.0, -size),
                    Vector::new(0.0, -padding),
                    Vector::new(size / 2.0, 0.0),
                ),
                Layout::Portrait => (
                    Vector::new(size, 0.0),
                    Vector::new(padding, 0.0),
                    Vector::new(0.0, -size / 2.0),
                ),
            };

            let mut unactive_background_elements = Vec::new();
            let mut active_background_elements = Vec::new();
            let mut red_elements = Vec::new();
            let mut blue_elements = Vec::new();
            let mut layer_elements = Vec::new();
            let mut layer_outline_elements = Vec::new();

            let line_width = screen_transform.render_width_no_user();

            for i in 0..n {
                let button_top_left = Vector::new(
                    -screen_transform.square_scale.x.powi(-1),
                    screen_transform.square_scale.y.powi(-1),
                ) + button_delta * i as f64;
                let button_bottom_right = button_top_left + Vector::new(size, -size);

                // Background
                let background_elements = match i {
                    0..=2 => match (i, &mode) {
                        (0, Mode::Ruler(_)) | (1, Mode::Compass(_)) | (2, Mode::Eraser) => {
                            &mut active_background_elements
                        }
                        _ => &mut unactive_background_elements,
                    },
                    i => {
                        if current_layer == i - 3 {
                            &mut active_background_elements
                        } else {
                            &mut unactive_background_elements
                        }
                    }
                };
                background_elements.push(Element::line_segment(
                    button_top_left + (padding_delta + background_stroke_delta),
                    button_bottom_right - (padding_delta + background_stroke_delta),
                ));

                let origin = button_top_left + Vector::new(size / 2.0, -size / 2.0);

                // Tools
                let (mut red, mut blue) = match i {
                    // Line tool
                    0 => {
                        let end_delta = Vector::new(size / 2.0, size / 2.0)
                            - Vector::new(2.0 * padding, 2.0 * padding);
                        (
                            vec![Element::line_segment(
                                origin - end_delta,
                                origin + end_delta,
                            )],
                            vec![
                                Element::circle(origin - end_delta, (line_width / 2.0).powi(2)),
                                Element::circle(origin + end_delta, (line_width / 2.0).powi(2)),
                            ],
                        )
                    }
                    // Circle tool
                    1 => {
                        let r = size / 2.0 - 2.0 * padding;
                        (
                            vec![Element::circle(origin, r.powi(2))],
                            vec![
                                Element::circle(origin, (line_width / 2.0).powi(2)),
                                Element::circle(
                                    origin + Vector::new(r, 0.0),
                                    (line_width / 2.0).powi(2),
                                ),
                            ],
                        )
                    }
                    // TODO: Eraser tool
                    2 => (vec![], vec![]),
                    _ => (vec![], vec![]),
                };
                red_elements.append(&mut red);
                blue_elements.append(&mut blue);

                // Layers
                if i > 2 {
                    layer_elements.push(Element::circle(origin, (size / 4.0 - padding).powi(2)));
                    layer_outline_elements.push(Element::circle(
                        origin,
                        (size / 2.0 - 2.0 * padding).powi(2),
                    ))
                }
            }

            renderer.render_layer(
                &unactive_background_elements,
                size - 2.0 * padding,
                GRAY,
                transform,
            );
            renderer.render_layer(
                &active_background_elements,
                size - 2.0 * padding,
                WHITE,
                transform,
            );
            renderer.render_layer(&red_elements, line_width, RED, transform);
            renderer.render_layer(&blue_elements, line_width, BLUE, transform);
            for (i, element) in layer_elements.into_iter().enumerate() {
                renderer.render_layer(
                    &vec![element],
                    size / 2.0 - 2.0 * padding,
                    layers[i].colour,
                    transform,
                )
            }
            renderer.render_layer(&layer_outline_elements, line_width, BLACK, transform);
        }
        Event::Size(resolution) => {
            screen_transform.resolution = resolution;
            if screen_transform.resolution.x > screen_transform.resolution.y {
                screen_transform.square_scale = Vector::new(
                    screen_transform.resolution.y / screen_transform.resolution.x,
                    1.0,
                );
                layout = Layout::Landscape;
            } else {
                screen_transform.square_scale = Vector::new(
                    1.0,
                    screen_transform.resolution.x / screen_transform.resolution.y,
                );
                layout = Layout::Portrait;
            }
        }
        Event::Moved(screen_position) => {
            cursor.update_position(&mut screen_transform, screen_position);
            cursor.update_interaction(&screen_transform, &layers, &mode, ortho);
        }
        Event::Pressed(screen_position) => {
            cursor.update_position(&mut screen_transform, screen_position);
            cursor.update_interaction(&screen_transform, &layers, &mode, ortho);
        }
        Event::Released(screen_position) => {
            cursor.update_position(&mut screen_transform, screen_position);
            cursor.update_interaction(&screen_transform, &layers, &mode, ortho);
            if cursor.dragging {
                cursor.dragging = false;
                return;
            }
            let buttons_top_left = Vector::new(
                -screen_transform.square_scale.x.powi(-1),
                screen_transform.square_scale.y.powi(-1),
            );
            let size = 2.0 / 10.0;
            let buttons_bottom_right = buttons_top_left
                + match layout {
                    Layout::Landscape => Vector::new(size, -2.0),
                    Layout::Portrait => Vector::new(2.0, -size),
                };
            let uv = (screen_position / screen_transform.resolution * Vector::new(2.0, -2.0)
                + Vector::new(-1.0, 1.0))
                / screen_transform.square_scale;
            if uv.x < buttons_bottom_right.x && uv.y > buttons_bottom_right.y {
                let i = match layout {
                    Layout::Landscape => (-uv.y + 1.0) / size,
                    Layout::Portrait => (uv.x + 1.0) / size,
                } as usize;
                match i {
                    0 => mode = Mode::Ruler(None),
                    1 => mode = Mode::Compass(None),
                    2 => mode = Mode::Eraser,
                    i => current_layer = i - 3,
                }
                return;
            }
            mode = match mode {
                Mode::Ruler(None) => Mode::Ruler(Some(cursor.graph)),
                Mode::Ruler(Some(point)) => {
                    layers[current_layer]
                        .elements
                        .push(Element::line_segment(point, cursor.graph));
                    Mode::Ruler(None)
                }
                Mode::Compass(None) => Mode::Compass(Some(cursor.graph)),
                Mode::Compass(Some(center)) => {
                    layers[current_layer]
                        .elements
                        .push(Element::circle(center, (cursor.graph - center).mag_sq()));
                    Mode::Compass(None)
                }
                Mode::Eraser => {
                    while let Some((l, i)) = cursor.hovering.pop() {
                        layers[l].elements.swap_remove(i);
                    }
                    Mode::Eraser
                }
            };
            cursor.update_interaction(&screen_transform, &layers, &mode, ortho);
        }
        Event::Dragged(delta) => {
            cursor.dragging = true;
            let uv = delta / screen_transform.resolution * Vector::new(2.0, -2.0);
            screen_transform.translation += uv;
        }
        Event::Zoom(delta) => {
            screen_transform.user_scale *= delta;
            screen_transform.translation *= delta;
            screen_transform.translation += cursor.screen * (1.0 - delta);
            cursor.update_interaction(&screen_transform, &layers, &mode, ortho);
        }
        Event::Key(key) => {
            match key {
                'o' => ortho = !ortho,
                'r' => mode = Mode::Ruler(None),
                'c' => mode = Mode::Compass(None),
                'e' => mode = Mode::Eraser,
                n @ '1'..='7' => current_layer = n as usize - '1' as usize,
                '!' => layers[0].enabled = !layers[0].enabled,
                '@' => layers[1].enabled = !layers[1].enabled,
                '#' => layers[2].enabled = !layers[2].enabled,
                '$' => layers[3].enabled = !layers[3].enabled,
                '%' => layers[4].enabled = !layers[4].enabled,
                '^' => layers[5].enabled = !layers[5].enabled,
                '&' => layers[6].enabled = !layers[6].enabled,
                _ => (),
            }
            cursor.update_interaction(&screen_transform, &layers, &mode, ortho)
        }
    });
}
