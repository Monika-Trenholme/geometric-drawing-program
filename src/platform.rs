use crate::math::Vector;
use crate::renderer::Renderer;
use std::collections::HashMap;
use winit::event::{Event as SystemEvent, TouchPhase, WindowEvent as SystemWindowEvent};
use winit::event_loop::{
    EventLoop as SystemEventLoop, EventLoopWindowTarget as SystemEventLoopWindowTarget,
};

pub enum Event<'a> {
    Render(&'a mut Renderer),
    Size(Vector),
    Moved(Vector),
    Dragged(Vector),
    Pressed(Vector),
    Released(Vector),
    Zoom(f64),
    Key(char),
}

#[derive(Clone, Copy)]
struct Touch {
    phase: TouchPhase,
    current: Vector,
    previous: Option<Vector>,
}

pub trait Platform: Sized + 'static {
    fn init(title: &'static str, event_loop: SystemEventLoop<()>) -> Self;
    fn resumed(&mut self, window_target: &SystemEventLoopWindowTarget<()>);
    fn suspended(&mut self);
    fn request_redraw(&self);
    fn render<C>(&mut self, callback: &mut C)
    where
        C: 'static + FnMut(Event<'_>);
    fn resize(&self, size: winit::dpi::PhysicalSize<u32>);
    fn size(&self) -> Option<Vector>;
    fn take_event_loop(&mut self) -> SystemEventLoop<()>;

    fn run<C>(mut self, mut callback: C)
    where
        C: 'static + FnMut(Event<'_>),
    {
        let mut cursor_position = Vector::new(0.0, 0.0);
        let mut mouse_pressed = false;
        let mut touches: HashMap<u64, Touch> = HashMap::new();
        let mut multi_touch = false;
        let event_loop = self.take_event_loop();
        event_loop.run(move |event, window_target, control_flow| {
            control_flow.set_wait();
            match event {
                SystemEvent::Resumed => {
                    self.resumed(window_target);
                    if let Some(size) = self.size() {
                        (callback)(Event::Size(size));
                    }
                }
                SystemEvent::Suspended => self.suspended(),
                SystemEvent::MainEventsCleared => {
                    match touches.len() {
                        1 => {
                            let (&id, touch) = touches.iter().next().unwrap();
                            match touch.phase {
                                TouchPhase::Started if !multi_touch => {
                                    (callback)(Event::Pressed(touch.current))
                                }
                                TouchPhase::Moved if !multi_touch => {
                                    cursor_position = touch.current;
                                    (callback)(Event::Moved(touch.current));
                                }
                                TouchPhase::Ended => {
                                    if !multi_touch {
                                        (callback)(Event::Released(touch.current));
                                    }
                                    touches.remove(&id);
                                    multi_touch = false;
                                }
                                TouchPhase::Cancelled => {
                                    touches.remove(&id);
                                    multi_touch = false;
                                }
                                _ => (),
                            }
                        }
                        2 => {
                            multi_touch = true;
                            let mut iter = touches.iter_mut();
                            let (&id_a, touch_a) = iter.next().unwrap();
                            let (&id_b, touch_b) = iter.next().unwrap();

                            let previous_a = touch_a.previous.unwrap_or(touch_a.current);
                            let previous_b = touch_b.previous.unwrap_or(touch_b.current);

                            let previous_d = (previous_a - previous_b).mag();
                            let current_d = (touch_a.current - touch_b.current).mag();

                            (callback)(Event::Dragged(
                                (touch_a.current + touch_b.current) / 2.0
                                    - (previous_a + previous_b) / 2.0,
                            ));
                            (callback)(Event::Zoom(current_d / previous_d));

                            touch_a.previous = None;
                            touch_b.previous = None;

                            let phase_a = touch_a.phase;
                            let phase_b = touch_b.phase;

                            match phase_a {
                                TouchPhase::Ended | TouchPhase::Cancelled => {
                                    touches.remove(&id_a);
                                }
                                _ => (),
                            }
                            match phase_b {
                                TouchPhase::Ended | TouchPhase::Cancelled => {
                                    touches.remove(&id_b);
                                }
                                _ => (),
                            }
                        }
                        _ => (),
                    }
                    self.request_redraw();
                }
                SystemEvent::RedrawRequested(_) => self.render(&mut callback),
                SystemEvent::WindowEvent { event, .. } => match event {
                    SystemWindowEvent::CloseRequested => control_flow.set_exit(),
                    SystemWindowEvent::Resized(size) => {
                        self.resize(size);
                        (callback)(Event::Size(Vector::new(
                            size.width as f64,
                            size.height as f64,
                        )));
                    }
                    SystemWindowEvent::MouseInput {
                        state: element_state,
                        button,
                        ..
                    } => match (element_state, button) {
                        (winit::event::ElementState::Pressed, winit::event::MouseButton::Left) => {
                            mouse_pressed = true;
                            (callback)(Event::Pressed(cursor_position));
                        }
                        (winit::event::ElementState::Released, winit::event::MouseButton::Left) => {
                            mouse_pressed = false;
                            (callback)(Event::Released(cursor_position));
                        }
                        _ => (),
                    },
                    SystemWindowEvent::CursorMoved { position, .. } => {
                        let new_position = Vector::new(position.x, position.y);
                        (callback)(if mouse_pressed {
                            Event::Dragged(new_position - cursor_position)
                        } else {
                            Event::Moved(new_position)
                        });
                        cursor_position = new_position;
                    }
                    SystemWindowEvent::MouseWheel {
                        delta: winit::event::MouseScrollDelta::PixelDelta(delta),
                        ..
                    } => {
                        if delta.y > 0.0 {
                            (callback)(Event::Zoom(1.1));
                        } else {
                            (callback)(Event::Zoom((1.1_f64).powi(-1)));
                        }
                    }
                    SystemWindowEvent::ReceivedCharacter(c) => {
                        (callback)(Event::Key(c));
                    }
                    SystemWindowEvent::Touch(winit::event::Touch {
                        phase,
                        location,
                        id,
                        ..
                    }) => {
                        let v = Vector::new(location.x, location.y);
                        match phase {
                            TouchPhase::Started => {
                                touches.insert(
                                    id,
                                    Touch {
                                        phase,
                                        current: v,
                                        previous: None,
                                    },
                                );
                            }
                            _ => {
                                let touch = touches.get_mut(&id).unwrap();
                                touch.phase = phase;
                                touch.previous = Some(touch.current);
                                touch.current = v;
                            }
                        }
                    }
                    _ => (),
                },
                _ => (),
            }
        });
    }
}

#[cfg(not(target_arch = "wasm32"))]
mod glutin;
#[cfg(not(target_arch = "wasm32"))]
pub use self::glutin::NativePlatform;
#[cfg(target_arch = "wasm32")]
mod web_sys;
#[cfg(target_arch = "wasm32")]
pub use self::web_sys::NativePlatform;
