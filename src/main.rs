// Desktop / Web entry point
#[cfg(not(target_os = "android"))]
mod entry;
#[cfg(not(target_os = "android"))]
use entry::enter;
#[cfg(not(target_os = "android"))]
use geo_construction::platform::{NativePlatform, Platform};
#[cfg(not(target_os = "android"))]
use winit::event_loop::EventLoop;

#[cfg(not(target_os = "android"))]
pub fn main() {
    enter(NativePlatform::init("Geo Construction", EventLoop::new()));
}
