use std::ops;

fn nearly_zero(value: f64) -> bool {
    value.abs() < f64::EPSILON * 1000.0
}

#[derive(Clone, Copy)]
pub struct Vector {
    pub x: f64,
    pub y: f64,
}

impl Vector {
    pub fn new(x: f64, y: f64) -> Self {
        Self { x, y }
    }

    pub fn mag(self) -> f64 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }

    pub fn mag_sq(self) -> f64 {
        self.x.powi(2) + self.y.powi(2)
    }

    pub fn normalise(self) -> Self {
        self / self.mag()
    }

    pub fn dot(self, other: Self) -> f64 {
        self.x * other.x + self.y * other.y
    }
}

impl ops::Add for Vector {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl ops::AddAssign for Vector {
    fn add_assign(&mut self, other: Self) {
        self.x += other.x;
        self.y += other.y;
    }
}

impl ops::Sub for Vector {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl ops::SubAssign for Vector {
    fn sub_assign(&mut self, other: Self) {
        self.x -= other.x;
        self.y -= other.y;
    }
}

impl ops::Mul for Vector {
    type Output = Self;
    fn mul(self, other: Self) -> Self {
        Self {
            x: self.x * other.x,
            y: self.y * other.y,
        }
    }
}

impl ops::MulAssign for Vector {
    fn mul_assign(&mut self, other: Self) {
        self.x *= other.x;
        self.y *= other.y;
    }
}

impl ops::Mul<f64> for Vector {
    type Output = Self;
    fn mul(self, scale: f64) -> Self {
        Self {
            x: scale * self.x,
            y: scale * self.y,
        }
    }
}

impl ops::MulAssign<f64> for Vector {
    fn mul_assign(&mut self, scale: f64) {
        self.x *= scale;
        self.y *= scale;
    }
}

impl ops::Div for Vector {
    type Output = Self;
    fn div(self, other: Self) -> Self {
        Self {
            x: self.x / other.x,
            y: self.y / other.y,
        }
    }
}

impl ops::DivAssign for Vector {
    fn div_assign(&mut self, other: Self) {
        self.x /= other.x;
        self.y /= other.y;
    }
}

impl ops::Div<f64> for Vector {
    type Output = Self;
    fn div(self, scale: f64) -> Self {
        Self {
            x: self.x / scale,
            y: self.y / scale,
        }
    }
}

impl ops::DivAssign<f64> for Vector {
    fn div_assign(&mut self, scale: f64) {
        self.x /= scale;
        self.y /= scale;
    }
}

impl ops::Neg for Vector {
    type Output = Vector;
    fn neg(self) -> Self {
        Self {
            x: -self.x,
            y: -self.y,
        }
    }
}

#[derive(Clone, Copy)]
pub enum Element {
    // First number is slope, second intercept.
    // If the slope is Some(defined) the intercept is the y intercept,
    // otherwise it is the x intercept.
    Line(Option<f64>, f64),
    // End points, sorted by x value if they differ, otherwise sorted by y
    LineSegment(Vector, Vector),
    // Center and squared radius
    Circle(Vector, f64),
}

impl Element {
    pub fn line(a: Vector, b: Vector) -> Self {
        if nearly_zero(a.x - b.x) {
            Self::Line(None, a.x)
        } else {
            let m = (a.y - b.y) / (a.x - b.x);
            let c = a.y - m * a.x;
            Self::Line(Some(m), c)
        }
    }

    pub fn line_segment(a: Vector, b: Vector) -> Self {
        if nearly_zero(a.x - b.x) {
            if b.y > a.y {
                Self::LineSegment(a, b)
            } else {
                Self::LineSegment(b, a)
            }
        } else {
            if b.x > a.x {
                Self::LineSegment(a, b)
            } else {
                Self::LineSegment(b, a)
            }
        }
    }

    pub fn circle(c: Vector, r: f64) -> Self {
        Self::Circle(c, r)
    }

    pub fn contains_point(&self, width: f64, point: Vector) -> bool {
        match self {
            &Element::LineSegment(a, b) => {
                let d = b - a;
                let point_translated = point - a;
                let length = d.mag();
                let i = d / length;
                let x = i.dot(point_translated);

                // Projection onto line is within the segment
                x >= 0.0 && x <= length
				// Orthogonal is within the width
				&& (point_translated - i*x).mag() <= width / 2.0
            }
            &Element::Circle(c, rsq) => {
                let r = rsq.sqrt();
                let d = point - c;
                let distance = d.mag();

                // Further from center than inner radius
                distance >= r - width / 2.0
				// Closer to center than outer radius
				&& distance <= r + width / 2.0
            }
            _ => unreachable!(),
        }
    }
}

impl ops::BitAnd for &Element {
    // Returns vector with intersection points.
    // In the case of circle intersections it may return two duplicate points.
    type Output = Vec<Vector>;
    fn bitand(self, rhs: Self) -> Self::Output {
        match (self, rhs) {
            // Line on line intersection
            (&Element::Line(am, ac), &Element::Line(bm, bc)) => match (am, bm) {
                (None, None) => Vec::new(),
                (None, Some(bm)) => vec![Vector::new(ac, bm * ac + bc)],
                (Some(am), None) => vec![Vector::new(bc, am * bc + ac)],
                (Some(am), Some(bm)) => {
                    let dm = bm - am;
                    if nearly_zero(dm) {
                        Vec::new()
                    } else {
                        let x = (ac - bc) / (bm - am);
                        let y = am * x + ac;
                        vec![Vector::new(x, y)]
                    }
                }
            },
            // Line on circle intersection
            (&Element::Line(lm, lc), &Element::Circle(center, rsq))
            | (&Element::Circle(center, rsq), &Element::Line(lm, lc)) => {
                // If line is vertical
                if lm.is_none() {
                    let dx = lc - center.x;
                    if dx.powi(2) > rsq {
                        return Vec::new();
                    }
                    let dy = (rsq - dx.powi(2)).sqrt();
                    return [dy, -dy]
                        .into_iter()
                        .map(|y| Vector::new(lc.clone(), y + center.y))
                        .collect::<Vec<Vector>>();
                }
                // If line is not vertical
                let lm = lm.unwrap();
                let a = lm.powi(2) + 1.0;
                let b = 2.0 * (lm * (lc - center.y) - center.x);
                let c = (lc - center.y).powi(2) + center.x.powi(2) - rsq;
                let discriminant = b.powi(2) - 4.0 * a * c;
                if discriminant >= 0.0 {
                    let root = discriminant.sqrt();
                    [root, -root]
                        .into_iter()
                        .map(|root| {
                            let x = (-b + root) / (2.0 * a);
                            let y = lm * x + lc;
                            Vector::new(x, y)
                        })
                        .collect::<Vec<Vector>>()
                } else {
                    Vec::new()
                }
            }
            // Intersection involving line segment
            (&Element::LineSegment(a, b), other) | (other, &Element::LineSegment(a, b)) => {
                // Turn segment into line that extends infintely for calculation
                let line = Element::line(a, b);
                let m = if let Element::Line(m, _) = line {
                    m
                } else {
                    unreachable!()
                };
                let slope = m.is_some();
                (&line & other)
					.into_iter()
					.filter(|p|
						// Check if point is with in domain of the segment
						if slope {
							a.x < p.x && p.x < b.x
						} else {
							a.y < p.y && p.y < b.y
						}
					)
					.collect()
            }
            // Circle on circle intersection
            (&Element::Circle(c1, r1sq), &Element::Circle(c2, r2sq)) => {
                let line = if nearly_zero(c1.y - c2.y) {
                    let x = (c2.x.powi(2) - c1.x.powi(2) + r1sq - r2sq) / (2.0 * (c2.x - c1.x));
                    Element::Line(None, x)
                } else {
                    let dy = c2.y - c1.y;
                    let m = (c1.x - c2.x) / dy;
                    let c = (c2.mag_sq() - c1.mag_sq() + r1sq - r2sq) / (2.0 * dy);
                    Element::Line(Some(m), c)
                };
                &Element::Circle(c2, r2sq) & &line
            }
        }
    }
}
