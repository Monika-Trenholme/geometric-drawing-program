use crate::math::{Element, Vector};
use glow::*;

// Capacity of graphics buffer
const CAPACITY_VERTICES: usize = 1024;

#[derive(Clone, Copy)]
pub struct Colour(pub f32, pub f32, pub f32);

trait Vertex: Clone {
    unsafe fn attributes(context: &Context);
}

#[derive(Clone)]
struct LineVertex {
    position: (f32, f32),
}

impl LineVertex {
    fn new(position: Vector) -> Self {
        Self {
            position: (position.x as f32, position.y as f32),
        }
    }
}

impl Vertex for LineVertex {
    unsafe fn attributes(context: &Context) {
        // Position
        context.enable_vertex_attrib_array(0);
        context.vertex_attrib_pointer_f32(
            0,
            2,
            FLOAT,
            false,
            std::mem::size_of::<LineVertex>() as i32,
            0,
        );
    }
}

#[derive(Clone)]
struct ArcVertex {
    position: (f32, f32),
    center: (f32, f32),
    radius: f32,
    width: f32,
}

impl ArcVertex {
    fn new(position: Vector, center: Vector, radius: f64, width: f64) -> Self {
        Self {
            position: (position.x as f32, position.y as f32),
            center: (center.x as f32, center.y as f32),
            radius: radius as f32,
            width: width as f32,
        }
    }
}

impl Vertex for ArcVertex {
    unsafe fn attributes(context: &Context) {
        // Position
        context.enable_vertex_attrib_array(0);
        context.vertex_attrib_pointer_f32(
            0,
            2,
            FLOAT,
            false,
            std::mem::size_of::<ArcVertex>() as i32,
            0,
        );

        // Arc Center
        context.enable_vertex_attrib_array(1);
        context.vertex_attrib_pointer_f32(
            1,
            2,
            FLOAT,
            false,
            std::mem::size_of::<ArcVertex>() as i32,
            std::mem::size_of::<(f32, f32)>() as i32,
        );

        // Arc Radius
        context.enable_vertex_attrib_array(2);
        context.vertex_attrib_pointer_f32(
            2,
            1,
            FLOAT,
            false,
            std::mem::size_of::<ArcVertex>() as i32,
            std::mem::size_of::<((f32, f32), (f32, f32))>() as i32,
        );

        // Arc stroke width
        context.enable_vertex_attrib_array(3);
        context.vertex_attrib_pointer_f32(
            3,
            1,
            FLOAT,
            false,
            std::mem::size_of::<ArcVertex>() as i32,
            std::mem::size_of::<((f32, f32), (f32, f32), f32)>() as i32,
        );
    }
}

struct GraphicsBuffer<V: Vertex> {
    vertices: Vec<V>,
    indices: Vec<u16>,

    transform: [f64; 9],
    transform_location: Option<UniformLocation>,
    colour: Colour,
    colour_location: Option<UniformLocation>,

    vertex_buffer: Buffer,
    index_buffer: Buffer,
    vertex_array: VertexArray,
    program: Program,
}

impl<V: Vertex> GraphicsBuffer<V> {
    unsafe fn new(
        context: &Context,
        shader_version: &'static str,
        vertex_shader: &'static str,
        fragment_shader: &'static str,
    ) -> Self {
        let program = context.create_program().expect("Cannot create program");

        let sources = [
            (VERTEX_SHADER, vertex_shader),
            (FRAGMENT_SHADER, fragment_shader),
        ];

        let mut shaders = Vec::new();
        for (shader_type, shader_source) in sources {
            let shader = context
                .create_shader(shader_type)
                .expect("Cannot create shader");
            context.shader_source(shader, &format!("{shader_version}\n{shader_source}"));
            context.compile_shader(shader);
            if !context.get_shader_compile_status(shader) {
                panic!("{}", context.get_shader_info_log(shader));
            }
            context.attach_shader(program, shader);
            shaders.push(shader);
        }

        context.link_program(program);
        if !context.get_program_link_status(program) {
            panic!("{}", context.get_program_info_log(program));
        }

        for shader in shaders {
            context.detach_shader(program, shader);
            context.delete_shader(shader);
        }

        let vertex_buffer = context.create_buffer().unwrap();
        context.bind_buffer(ARRAY_BUFFER, Some(vertex_buffer));
        context.buffer_data_size(
            ARRAY_BUFFER,
            std::mem::size_of::<V>() as i32 * CAPACITY_VERTICES as i32,
            DYNAMIC_DRAW,
        );

        let vertex_array = context.create_vertex_array().unwrap();
        context.bind_vertex_array(Some(vertex_array));
        V::attributes(&context);

        let index_buffer = context.create_buffer().unwrap();
        context.bind_buffer(ELEMENT_ARRAY_BUFFER, Some(index_buffer));
        context.buffer_data_size(
            ELEMENT_ARRAY_BUFFER,
            std::mem::size_of::<u16>() as i32 * CAPACITY_VERTICES as i32 * 3 / 2,
            DYNAMIC_DRAW,
        );

        let transform_location = context.get_uniform_location(program, "transform");
        let colour_location = context.get_uniform_location(program, "colour");

        Self {
            vertices: Vec::with_capacity(CAPACITY_VERTICES),
            indices: Vec::with_capacity(CAPACITY_VERTICES * 3 / 2),

            transform: [0.0; 9],
            transform_location,
            colour: Colour(1.0, 1.0, 1.0),
            colour_location,

            vertex_buffer,
            index_buffer,
            vertex_array,
            program,
        }
    }

    fn set_transform(&mut self, transform: [f64; 9]) {
        self.transform = transform;
    }

    fn set_colour(&mut self, colour: Colour) {
        self.colour = colour;
    }

    unsafe fn push_element(&mut self, context: &Context, vertices: &[V], indices: &[u8]) {
        if self.vertices.len() + vertices.len() > self.vertices.capacity() {
            self.flush(context);
        }
        let index_offset = self.vertices.len() as u16;
        self.indices
            .extend(indices.iter().cloned().map(|i| i as u16 + index_offset));
        self.vertices.extend_from_slice(vertices);
    }

    unsafe fn flush(&mut self, context: &Context) {
        context.bind_vertex_array(Some(self.vertex_array));

        context.bind_buffer(ARRAY_BUFFER, Some(self.vertex_buffer));
        context.buffer_sub_data_u8_slice(
            ARRAY_BUFFER,
            0,
            std::slice::from_raw_parts(
                self.vertices.as_ptr() as *const u8,
                std::mem::size_of::<V>() * self.vertices.len(),
            ),
        );

        context.bind_buffer(ELEMENT_ARRAY_BUFFER, Some(self.index_buffer));
        context.buffer_sub_data_u8_slice(
            ELEMENT_ARRAY_BUFFER,
            0,
            std::slice::from_raw_parts(
                self.indices.as_ptr() as *const u8,
                std::mem::size_of::<u16>() * self.indices.len(),
            ),
        );

        context.use_program(Some(self.program));
        context.uniform_matrix_3_f32_slice(
            self.transform_location.as_ref(),
            false,
            &self
                .transform
                .into_iter()
                .map(|f| f as f32)
                .collect::<Vec<f32>>(),
        );
        context.uniform_3_f32_slice(
            self.colour_location.as_ref(),
            std::mem::transmute::<&Colour, &[f32; 3]>(&self.colour),
        );
        context.draw_elements(TRIANGLES, self.indices.len() as i32, UNSIGNED_SHORT, 0);

        self.vertices.clear();
        self.indices.clear();
    }
}

pub struct Renderer {
    context: Context,
    line_buffer: GraphicsBuffer<LineVertex>,
    arc_buffer: GraphicsBuffer<ArcVertex>,
}

impl Renderer {
    pub unsafe fn new(context: Context, shader_version: &'static str) -> Self {
        let line_buffer = GraphicsBuffer::<LineVertex>::new(
            &context,
            shader_version,
            r#"
        	layout(location = 0) in vec2 in_position;
        	uniform mat3 transform;
        	void main() {
        		gl_Position = vec4((vec3(in_position, 1.0) * transform).xy, 0.0, 1.0);
        	}
        	"#,
            r#"
        	precision mediump float;
        	uniform vec3 colour;
        	out vec4 out_colour;
        	void main() {
        		out_colour = vec4(colour, 1.0);
        	}
        	"#,
        );

        let arc_buffer = GraphicsBuffer::<ArcVertex>::new(
            &context,
            shader_version,
            r#"
			layout(location = 0) in vec2 in_position;
			layout(location = 1) in vec2 in_center;
			layout(location = 2) in float in_radius;
			layout(location = 3) in float in_width;
			uniform mat3 transform;
			out vec2 position;
			out vec2 center;
			out vec2 inner_radius;
			out vec2 outer_radius;
			void main() {
				position = (vec3(in_position, 1.0) * transform).xy;
				center = (vec3(in_center, 1.0) * transform).xy;
				vec2 scale = vec2(transform[0][0], transform[1][1]);
				inner_radius = (in_radius - in_width / 2.0) * scale;
				outer_radius = (in_radius + in_width / 2.0) * scale;
				gl_Position = vec4(position, 0.0, 1.0);
			}
			"#,
            r#"
			precision mediump float;
			in vec2 position;
			in vec2 center;
			in vec2 inner_radius;
			in vec2 outer_radius;
			uniform vec3 colour;
			out vec4 out_colour;
			void main() {
				vec2 d = position - center;
				vec2 inner_v = d / inner_radius;
				vec2 outer_v = d / outer_radius;
				float inner_step = step(1.0, dot(inner_v, inner_v));
				float outer_step = step(0.0, 1.0 - dot(outer_v, outer_v));
				out_colour = vec4(colour, inner_step * outer_step);
			}
			"#,
        );

        context.clear_color(0.0, 0.0, 0.0, 1.0);
        context.blend_func(SRC_ALPHA, ONE_MINUS_SRC_ALPHA);
        context.enable(BLEND);

        Self {
            context,
            line_buffer,
            arc_buffer,
        }
    }

    pub fn viewport(&self, x: i32, y: i32, width: i32, height: i32) {
        unsafe {
            self.context.viewport(x, y, width, height);
        }
    }

    pub fn clear(&self) {
        unsafe {
            self.context.clear(COLOR_BUFFER_BIT);
        }
    }

    pub fn render_layer(
        &mut self,
        elements: &Vec<Element>,
        width: f64,
        colour: Colour,
        transform: [f64; 9],
    ) {
        self.line_buffer.set_transform(transform);
        self.arc_buffer.set_transform(transform);
        self.line_buffer.set_colour(colour);
        self.arc_buffer.set_colour(colour);

        for element in elements {
            match element {
                &Element::LineSegment(a, b) => unsafe {
                    let d = b - a;
                    let normal = Vector::new(-d.y, d.x).normalise() * width / 2.0;
                    self.line_buffer.push_element(
                        &self.context,
                        &[
                            LineVertex::new(a + normal),
                            LineVertex::new(a - normal),
                            LineVertex::new(b + normal),
                            LineVertex::new(b - normal),
                        ],
                        &[0, 1, 2, 2, 1, 3],
                    );
                },
                &Element::Circle(c, rsq) => unsafe {
                    let r = rsq.sqrt();
                    let pad = r + width / 2.0;
                    self.arc_buffer.push_element(
                        &self.context,
                        &[
                            ArcVertex::new(c + Vector::new(-pad, pad), c, r, width),
                            ArcVertex::new(c + Vector::new(pad, pad), c, r, width),
                            ArcVertex::new(c + Vector::new(-pad, -pad), c, r, width),
                            ArcVertex::new(c + Vector::new(pad, -pad), c, r, width),
                        ],
                        &[0, 2, 1, 2, 3, 1],
                    );
                },
                _ => unreachable!(),
            }
        }

        unsafe {
            if self.line_buffer.vertices.len() > 0 {
                self.line_buffer.flush(&self.context);
            }
            if self.arc_buffer.vertices.len() > 0 {
                self.arc_buffer.flush(&self.context);
            }
        }
    }
}

impl Drop for Renderer {
    fn drop(&mut self) {
        unsafe {
            self.context.delete_program(self.line_buffer.program);
            self.context.delete_program(self.arc_buffer.program);
            self.context.delete_buffer(self.line_buffer.vertex_buffer);
            self.context.delete_buffer(self.arc_buffer.vertex_buffer);
            self.context.delete_buffer(self.line_buffer.index_buffer);
            self.context.delete_buffer(self.arc_buffer.index_buffer);
            self.context
                .delete_vertex_array(self.line_buffer.vertex_array);
            self.context
                .delete_vertex_array(self.arc_buffer.vertex_array);
        }
    }
}
