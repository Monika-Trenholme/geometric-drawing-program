// Make modules available to Desktop / Web entry point
pub mod math;
pub mod platform;
pub mod renderer;

// Android entry point
#[cfg(target_os = "android")]
use platform::{NativePlatform, Platform};
#[cfg(target_os = "android")]
mod entry;
#[cfg(target_os = "android")]
use entry::enter;
#[cfg(target_os = "android")]
use winit::{
    event_loop::EventLoopBuilder,
    platform::android::{activity::AndroidApp, EventLoopBuilderExtAndroid},
};

#[cfg(target_os = "android")]
#[no_mangle]
fn android_main(app: AndroidApp) {
    enter(NativePlatform::init(
        "Geo Construction",
        EventLoopBuilder::with_user_event()
            .with_android_app(app.clone())
            .build(),
    ));
}
