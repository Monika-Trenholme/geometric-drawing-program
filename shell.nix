{ pkgs ? import <nixpkgs> {config.android_sdk.accept_license = true;}, lib ? pkgs.lib }:
let
	androidComposition = pkgs.androidenv.composeAndroidPackages {
		toolsVersion = "26.1.1";
		platformToolsVersion = "31.0.3";
		buildToolsVersions = [ "30.0.3" ];
		includeEmulator = false;
		emulatorVersion = "30.3.4";
		platformVersions = [ "28" "29" "30" ];
		includeSources = false;
		includeSystemImages = false;
		systemImageTypes = [ "google_apis_playstore" ];
		abiVersions = [ "armeabi-v7a" "arm64-v8a" ];
		cmakeVersions = [ "3.10.2" ];
		includeNDK = true;
		ndkVersions = ["22.0.7026061"];
		useGoogleAPIs = false;
		useGoogleTVAddOns = false;
		includeExtras = [
			"extras;google;gcm"
		];
	};
in
with pkgs; mkShell rec {
  nativeBuildInputs = [
	libxkbcommon
	libGL
    
    # WINIT_UNIX_BACKEND=wayland
    wayland
    
    # WINIT_UNIX_BACKEND=x11
    xorg.libXcursor
    xorg.libXrandr
    xorg.libXi
    xorg.libX11

    #androidComposition
  ];

  LD_LIBRARY_PATH = "${lib.makeLibraryPath nativeBuildInputs}";
  ANDROID_HOME = "${androidComposition.androidsdk}/libexec/android-sdk";
  NDK_HOME = "${ANDROID_HOME}/ndk-bundle";
}
