web:
	cargo build --target wasm32-unknown-unknown
	~/.cargo/bin/wasm-bindgen target/wasm32-unknown-unknown/debug/geo-construction.wasm --out-dir web --target web

android:
	cargo apk build --lib

.PHONY: web android
